//
//  Constants.swift
//  Chat Uygulamam
//
//  Created by Kerim Çağlar on 08/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//


import Foundation
import FirebaseDatabase
import FirebaseStorage

struct Constants {
    
    static let dbRef:FIRDatabaseReference = FIRDatabase.database().reference()
    static let dbChats = dbRef.child("mesajlar")
    static let dbMedias = dbRef.child("gorseller")
    
    static let storageRef = FIRStorage.storage().reference(forURL:"gs://chat-udemy-app.appspot.com")
    static let imageStorageRef = storageRef.child("Resimler")
    static let videoStorageRef = storageRef.child("Videolar")
}
