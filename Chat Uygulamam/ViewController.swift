//
//  ViewController.swift
//  Chat Uygulamam
//
//  Created by Kerim Çağlar on 07/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import MobileCoreServices
import AVKit
import FirebaseStorage
import FirebaseDatabase
import SDWebImage


class ViewController: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var imagePicker = UIImagePickerController()
    var messages = [JSQMessage]()
    
    //Mesajlarımızın Rengini ayarlama, Bubbles image rengi ayarlama
    
    //Giden mesaj renk ayarı
    lazy var outgoingBubble:JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleGreen())
    }()
    
    //Gelen Mesaj renk ayarı
    lazy var incomingBubble:JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Başlangıç için bazı şeyleri gizleyelim
        //inputToolbar.contentView.leftBarButtonItem = nil // attachment butonunu gizleme
        
        inputToolbar.contentView.textView.placeHolder = "Yeni Mesaj Yaz"
        inputToolbar.contentView.rightBarButtonItem.setTitle("", for: UIControlState.normal)
        inputToolbar.contentView.rightBarButtonItem.setImage(#imageLiteral(resourceName: "send"), for: UIControlState.normal)
        
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        senderId = "99"
        senderDisplayName = "Hüseyin SAYGI"
        
        let lastMessages = Constants.dbChats.queryLimited(toLast: 20)
        lastMessages.observe(.childAdded, with:{ snapshot in
            if let data = snapshot.value as? [String:String],
                let senderId = data["senderId"],
                let displayName = data["senderName"],
                let text = data["mesaj"],
                !text.isEmpty{
                
                if let message = JSQMessage(senderId: senderId, displayName: displayName, text: text){
                    self.messages.append(message)
                    self.finishReceivingMessage()
                }
            }
        })
        
    }
    
    //CollectionView Methodlar
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        
        return messages[indexPath.item]
    }
    
    //Mesajın gelen mi giden mi olduğunu anlamamız için
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
        // a ? b : c ternary
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        return nil
    }
    
    //Gönderici ismi gönderme ayarı
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        return messages[indexPath.item].senderId == senderId ? nil : NSAttributedString(string:messages[indexPath.item].senderDisplayName)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        
        return messages[indexPath.item].senderId == senderId ? 0 : 20
    }
    
    //Gönder butonuna basınca yapılack işlemler
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        let ref = Constants.dbChats.childByAutoId() // Uniqe id oluşturur
        let message = ["senderId":senderId, "senderName": senderDisplayName, "mesaj": text]
        self.messages.append(JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text))
        
        collectionView.reloadData()
        ref.setValue(message)
        
        finishSendingMessage()
    }
    
    //Attachment ile birşey gönderilince çalışacak method
    override func didPressAccessoryButton(_ sender: UIButton!) {
        let actionSheet = UIAlertController(title: "Görsel Öğeler", message: "Lütfen bir görsel mesaj seçiniz", preferredStyle: .actionSheet)
        let resim = UIAlertAction(title: "Resimler", style: .default) { (action) in
            self.gorselSec(type: kUTTypeImage) // seçilen öğenin türü resim
        }
        let video = UIAlertAction(title: "Video", style: .default) { (action) in
            self.gorselSec(type: kUTTypeMovie) // seçilen öğenin türü video
        }
        
        let iptal = UIAlertAction(title: "İptal", style: .cancel, handler: nil)
        
        actionSheet.addAction(resim)
        actionSheet.addAction(video)
        actionSheet.addAction(iptal)
        
        self.present(actionSheet,animated:true,completion:nil)
    }
    
    func gorselSec(type:NSString){
        self.imagePicker.delegate = self // Delegate kod ile bağlantısı
        self.imagePicker.mediaTypes = [type as String]
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    //Resmin Seçilmesi,seçildikten sonra resim albümünün kapamnaması ve resmin mesaj olarak görünmesi
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let resim = info[UIImagePickerControllerOriginalImage] as? UIImage{
            //let image = JSQPhotoMediaItem(image:resim)
            //self.messages.append(JSQMessage(senderId: senderId, displayName: senderDisplayName, media: image))
            let data = UIImageJPEGRepresentation(resim, 0.05)
            self.gorselMesajGonderme(image: data, video: nil, senderId: senderId, senderName: senderDisplayName)
            
        } else if let video = info[UIImagePickerControllerMediaURL] as? URL {
            //let myVideo = JSQVideoMediaItem(fileURL: video, isReadyToPlay: true)
            //self.messages.append(JSQMessage(senderId: senderId, displayName: senderDisplayName, media: myVideo))
            self.gorselMesajGonderme(image: nil, video: video, senderId: senderId, senderName: senderDisplayName)
        }
        
        dismiss(animated: true, completion: nil)
        
        collectionView.reloadData()
    }
    
    //Videonun çalıştırılması için gerekenler
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        
        let message = messages[indexPath.item]
        
        if message.isMediaMessage{
            if let videoMesaj = message.media as? JSQVideoMediaItem{
                let oynatici = AVPlayer(url: videoMesaj.fileURL)
                let oynaticiKontroller = AVPlayerViewController()
                oynaticiKontroller.player = oynatici
                present(oynaticiKontroller, animated: true, completion: nil)
            }
        }
    }
    
    func gorselMesajKaydetme(senderId: String, senderName: String, url: String) {
        let data = ["senderId": self.senderId, "senderName": self.senderDisplayName, "url": url];
        print("************DATAAA:\(data)")
        Constants.dbMedias.childByAutoId().setValue(data)
    }
    
    func gorselMesajGonderme(image: Data?, video: URL?, senderId: String, senderName: String) {
        
        if image != nil {
            
            Constants.imageStorageRef.child(senderId + "\(NSUUID().uuidString).jpg").put(image!, metadata: nil) { (metadata: FIRStorageMetadata?, error: Error?) in
                
                if error != nil {
                    // Kullanıcıya hata olduğunu bildirin
                    print(error!)
                } else {
                    //let downloadUrl = metadata!.downloadURL()
                    //downloadUrl?.absoluteString
                    self.gorselMesajKaydetme(senderId: senderId, senderName: self.senderDisplayName, url: String(describing: metadata!.downloadURL()!))
                }
                
            }
            
        } else {
            Constants.videoStorageRef.child(senderId + "\(NSUUID().uuidString)").putFile(video!, metadata: nil) { (metadata: FIRStorageMetadata?, error: Error?) in
                
                if error != nil {
                    // Kullanıcıya hata olduğunu bildirin
                    print(error!)
                } else {
                    self.gorselMesajKaydetme(senderId: senderId, senderName: self.senderDisplayName, url: String(describing: metadata!.downloadURL()!))
                }
                
            }
        }
        
    }
    
}

